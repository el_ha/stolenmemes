FROM php:7.2-fpm-alpine

WORKDIR /var/www/html/public

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY --chown=www-data:www-data ./rest/meme.php ./rest/meme.php

USER www-data
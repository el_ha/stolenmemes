<?php

require("../vendor/autoload.php");

use Symfony\Component\Dotenv\Dotenv;
use Kreait\Firebase\Factory;

$dotenv = new Dotenv();
$dotenv->load(__DIR__ . '/../.env');

$factory = (new Factory)->withServiceAccount("./../" . $_ENV["SERVICE_ACCOUNT"])->withDatabaseUri($_ENV["DATABASE_URI"]);

$db = $factory->createDatabase();

$reference = $db->getReference("memes");

$snapshot = $reference->getSnapshot();

parse_str(file_get_contents("php://input"), $post_data);

if (isset($_SERVER["REQUEST_METHOD"]) &&  $_SERVER["REQUEST_METHOD"] !== "") {

	if ($_SERVER["REQUEST_METHOD"] === "GET") {
		if (isset($_GET["perPage"], $_GET["page"])) {
			["perPage" => $perPage, "page" => $page] = $_GET;
			$page = intval($page);
			$perPage = intval($perPage);
			$allData = $snapshot->getValue();
			$from = ($page - 1) * $perPage;
			$cut = array_slice($allData, $from, $from + $perPage);
			http_response_code(200);
			echo json_encode([
				"memes" => $cut,
				"count" => count($allData)
			]);
		} else {
			http_response_code(422);
			echo ("Missing Params");
		}
	}

	if ($_SERVER['REQUEST_METHOD'] === "POST") {
		if (isset($_POST["link"])) {
			list("link" => $link) = $_POST;
			if (filter_var($link, FILTER_VALIDATE_URL)) {
				$newObj = new stdClass;
				$newObj->link = $link;
				$reference->push($newObj);
				http_response_code(200);
				echo "ok";
			} else {
				http_response_code(422);
				echo "Unprocessable Entity";
			}
		} else {
			http_response_code(422);
			echo "Missing Param";
		}
	}
}
